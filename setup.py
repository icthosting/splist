from setuptools import setup

setup(
    name = "splist",
    version = '0.0.1',
    author = 'Stuart Esdaile',
    author_email = 'stuart.esdaile@sydney.edu.au',
    license = 'MIT',
    keywords = 'sharepoint list',
    description = 'Sharepoint List Modifier with NTLM auth',
    url = 'https://bitbucket.org/icthosting/splist',
    download_url = 'https://bitbucket.org/icthosting/splist/get/master.zip',
    py_modules=['splist','o365list'],
    install_requires = [
        'docopt',
        'boltons',
        'python-ntlm',
        'sharepoint',
        'Office365-REST-Python-Client'
    ],
    entry_points = {
        'console_scripts': [
            'splist=splist:main',
            'spolist=o365list:main'
        ]
    },
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ]
)
