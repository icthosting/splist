#!/usr/bin/env python
"""Sharepoint List Tool

    Querys and changes sharepoint lists. Simple changes can be done on the
    command line, while complex objects can be supplied by file or stdin.

Usage: splist [options] [--] [COMMAND [FIELD|(KEY VALUE)...]]

Commands:

    get:
        Returns rows matching KEY VALUE filters or
        existing values for FIELD ordered by most common.
    add:
        Adds new objects to a sharepoint list.
    mod:
        Modifies existing entries in a sharepoint list.


Options:
    -i FILE --ifn=FILE   Input file. [default: -]
    -o FILE --ofn=FILE   Output file. [default: -]
    -p --pretty          Pretty/Verbose Output.
    -j --json            Output as JSON.
    -u --uniquename      Ensure name field is unique.
    --namefield=NAME     Name field. [default: Title]
    -l NUM --limit=NUM   Limits the total number of returned. [default: 0]
    -H HOST --host=HOST  Sharepoint host. [default: {SP_HOST}]
    -S SITE --site=SITE  Site name. [default: {SP_SITE}]
    -T TEAM --team=TEAM  Team subsite name. [default: {SP_TEAM}]
    -L LIST --list=LIST  List. [default: {SP_LIST}]
    -D DOM --dom=DOM     Domain. [default: {SP_DOM}]
    -U USER --user=USER  Username. [default: {SP_USER}]
    -P PASS --pass=PASS  Keyring password override.
    -v --verbose         Verbose output where appropriate.
    -h --help            Show this.
    --version            Show version.
"""

from __future__ import unicode_literals, with_statement, division, absolute_import, print_function

__version__ = '0.0.1'

fields_to_ignore = '''Attachments Author BaseName ContentType ContentTypeId
Created Created_x0020_Date DocIcon Edit Editor EncodedAbsUrl FSObjType 
FileDirRef FileLeafRef FileRef File_x0020_Type FolderChildCount GUID _Level
HTML_x0020_File_x0020_Type ID InstanceID ItemChildCount Last_x0020_Modified
LinkFilename LinkFilename2 LinkFilenameNoMenu LinkTitle LinkTitle2 UniqueId
LinkTitleNoMenu MetaInfo Modified Order PermMask ProgId ScopeId SelectTitle
ServerUrl SortBehavior SyncClientId WorkflowInstanceID owshiddenversion
WorkflowVersion _CopySource _EditMenuTableEnd _EditMenuTableStart
_EditMenuTableStart2 _HasCopyDestinations _IsCurrentVersion _UIVersion
_ModerationComments _ModerationStatus _UIVersionString'''.split()

try:
    import gnomekeyring as gk
    import glib
    glib.set_application_name(str('splist'))
except:
    pass

import os
import sys
import json
import docopt
from boltons.dictutils import OMD
from collections import Hashable
from ntlm import HTTPNtlmAuthHandler
from sharepoint import SharePointSite, basic_auth_opener, lists, auth

def ntlm_auth_opener(url, username, password):
    password_manager = auth.HTTPPasswordMgrWithDefaultRealm()
    password_manager.add_password(None, url, username, password)
    auth_handler = HTTPNtlmAuthHandler.HTTPNtlmAuthHandler(password_manager)
    opener = auth.build_opener(auth_handler)
    return opener

def getsite(host, site, team, user, passwd='', dom=None, **opts):
    burl = str('https://{host}/'.format(host=host))
    opener = basic_auth_opener
    surl = '{burl}sites/{site}/{team}'.format(burl=burl,site=site,team=team)
    if dom:
        opener = ntlm_auth_opener
        user = str('{dom}\\{user}'.format(dom=dom,user=user))
    try:
        keys = gk.find_items_sync(gk.ITEM_GENERIC_SECRET,{str('username_value'):user,str('signon_realm'):burl})
        pwd = keys[0].secret if keys else ''
    except:
        pwd = ''
    return SharePointSite(surl, opener(burl, user, passwd or pwd))

def load_data(ifp=sys.stdin,ofp=sys.stderr,verbose=False):
    if ifp.isatty():
        raise IOError('Input is a tty')
    data = json.load(ifp)
    data = data if isinstance(data,(list,tuple)) else [data]
    if verbose:
        json.dump(data,ofp,indent=(None,4)[ofp.isatty()])
        print('',file=ofp)
    return data

def list_lists(site, ofp=sys.stdout, verbose=False, **opts):
    ofp.write('\n'.join(
        (verbose and lst.id+': ' or '')+lst.meta['Title']
        for lst in site.lists
        )+'\n')

def list_fields(lst, ofp=sys.stdout, verbose=False, **opts):
    ofp.write('\n'.join(sorted(f for f in lst.fields 
        if verbose or f not in fields_to_ignore))+'\n')

def cmd_get(lst, ofp=sys.stdout, verbose=False, **opts):
    if opts['FIELD']:
        vals = (getattr(r,opts['FIELD']) for r in lst.rows)
        keys = sorted(set(
            f if isinstance(f, Hashable) else json.dumps(f)
            for f in vals if f))
        ret = '\n'.join(keys[:int(opts['limit']) or len(keys)])
    else:
        filters = OMD(zip(opts['KEY'],opts['VALUE'])).todict(True).items()
        # print(filters,file=sys.stderr)
        fl = ['ID'] + sorted(set(lst.fields.keys()) - set(fields_to_ignore))
        rows = (r for r in lst.rows 
            if all(getattr(r,k) in v for k,v in filters))
        if opts['json']:
            ret = json.dumps([r.as_dict(field_names=fl) for r in rows],indent=[None,4][opts['pretty']],default='{0}'.format)
        else:
            fmt = ['','{0.id} {0.Created}: '][verbose]+'{0.name}'
            ret = '\n'.join(fmt.format(row) for row in rows)
    ofp.write(ret+'\n')

def cmd_add(lst, ifp=sys.stdin, ofp=sys.stdout, verbose=False, namefield='Title', **opts):
    data = load_data(ifp,ofp,verbose)
    if opts.get('uniquename'):
        names = set(d[namefield] for d in data)
        if len(names) < len(data):
            raise KeyError('Duplicate name[{0}] fields found'.format(namefield))
        for r in lst.rows:
            if r.name in names:
                raise KeyError('name already in use: {0.name}'.format(r))
    [lst.append(d) for d in data]
    lst.save()

def cmd_mod(lst, ifp=sys.stdin, ofp=sys.stdout, verbose=False, namefield='Title', **opts):
    data = load_data(ifp,ofp,verbose)
    if any(namefield not in d and not d.get('ID') for d in data):
        raise KeyError('Modifications require either an ID or {0} field'.format(namefield))
    if not all(d['ID'] in lst.rows_by_id for d in data if d.get('ID')):
        raise KeyError('ID fields must exist before being modified.')
    idlookup = dict((k,set(v)) for k,v in OMD((r.name, r.id) for r in lst.rows).todict(True).items())
    for i,d in enumerate(data):
        if 'ID' in d:
            if namefield in d:
                if opts.get('uniquename'):
                    if idlookup.get(d[namefield]):
                        if int(d['ID']) not in idlookup[d[namefield]]:
                            raise KeyError('[{0}] {1} not unique: {2}'.format(i,namefield,d[namefield]))
            r = lst.rows_by_id[int(d['ID'])]
            for k,v in d.items():
                if k!='ID':
                    if k==namefield:
                        print('[{0}] Old [{2.id}]{1}: {2.name}'.format(i,k,r),file=ofp)
                        idlookup[r.name].remove(r.id)
                        idlookup[v].add(r.id)
                    print('[{0}] Updating [{2.id}]{1}: {3}'.format(i,k,r,v),file=ofp)
                    setattr(r,k,v)
        elif namefield in d:
            if not idlookup.get(d[namefield]):
                raise KeyError('[{0}] Modifications based on {1} require {1} to exist'.format(i,namefield))
            rows = [lst.rows_by_id[r] for r in sorted(idlookup[d[namefield]])]
            if opts.get('uniquename') and len(rows)!=1:
                raise KeyError('[{0}] Duplicate {1} entries found: {2}'.format(i,namefield,', '.join(map('{0.id}'.format,rows))))
            for r in rows:
                for k,v in d.items():
                    if k not in ('ID',namefield):
                        print('[{0}] Updating [{2.id}]{1}: {3}'.format(i,k,r,v),file=ofp)
                        setattr(r,k,v)
        else:
            # Should never execute
            raise KeyError('[{0}] Modifications require either an ID or {1} field'.format(i,namefield))
    lst.save()

def cmd_del(lst, ifp=sys.stdin, ofp=sys.stdout, verbose=False, **opts):
    data = load_data(ifp,ofp,verbose)

def run(opts):
    # print(opts,file=sys.stderr)
    # return
    opts = dict((k.lstrip('-'),v) for k,v in opts.items())
    opts['passwd'] = opts['pass'] or os.environ.get('SP_PASS','')
    opts['verbose'] = opts['pretty'] or opts['verbose']
    site = getsite(**opts)
    opts['ifp'] = sys.stdin if opts['ifn']=='-' else open(opts['ifn'], 'r')
    opts['ofp'] = sys.stdout if opts['ofn']=='-' else open(opts['ofn'], 'w')
    if not opts['list']:
        if opts['verbose']:
            print("Lists",file=sys.stderr)
        list_lists(site, **opts)
    else:
        fn = globals().get('cmd_{0[COMMAND]}'.format(opts),list_fields)
        # print(fn.__name__.split('_')[-1].title(),file=sys.stderr)
        fn(site.lists[opts['list']], **opts)

def main():
    # defs = dict((x,os.environ.get(x,'')) for x in 'SP_HOST SP_SITE SP_TEAM SP_LIST SP_DOM SP_USER'.split())
    defs = {'SP_HOST':'','SP_SITE':'','SP_TEAM':'','SP_LIST':'','SP_DOM':'','SP_USER':''}
    defs.update(os.environ)
    # print(defs,file=sys.stderr)
    try:
        run(docopt.docopt(__doc__.format(**defs),version=__version__))
    except lists.etree.XMLSyntaxError:
        if os.environ.get('SP_DEBUG'):
            raise
    # except (KeyError,IOError) as e:
    #     print(e.message,file=sys.stderr)

if __name__ == '__main__':
    main()
