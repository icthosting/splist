#!/usr/bin/env python
"""Office 365 Sharepoint List Tool

    Querys and changes sharepoint lists. Simple changes can be done on the
    command line, while complex objects can be supplied by file or stdin.

Usage: o365list [options] [--] COMMAND [FIELD|(KEY VALUE)...]

Commands:

    get:
        Returns rows matching KEY VALUE filters or
        existing values for FIELD ordered by most common.
    add:
        Adds new objects to a sharepoint list.
    mod:
        Modifies existing entries in a sharepoint list.


Options:
    -i FILE --ifn=FILE   Input file. [default: -]
    -o FILE --ofn=FILE   Output file. [default: -]
    -p --pretty          Pretty/Verbose Output.
    -j --json            Output as JSON.
    -u --uniquename      Ensure name field is unique.
    --namefield=NAME     Name field. [default: Title]
    -l NUM --limit=NUM   Limits the total number of returned. [default: 0]
    -H HOST --host=HOST  Sharepoint host. [default: {SP_HOST}]
    -S SITE --site=SITE  Site name. [default: {SP_SITE}]
    -T TEAM --team=TEAM  Team subsite name. [default: {SP_TEAM}]
    -L LIST --list=LIST  List. [default: {SP_LIST}]
    -U USER --user=USER  Username. [default: {SP_USER}]
    -P PASS --pass=PASS  Keyring password override.
    -v --verbose         Verbose output where appropriate.
    -h --help            Show this.
    --version            Show version.
"""

from __future__ import unicode_literals, with_statement, division, absolute_import, print_function

__version__ = '0.0.1'

fields_to_ignore = '''Attachments Author BaseName ContentType ContentTypeId
Created Created_x0020_Date DocIcon Edit Editor EncodedAbsUrl FSObjType 
FileDirRef FileLeafRef FileRef File_x0020_Type FolderChildCount GUID _Level
HTML_x0020_File_x0020_Type ID InstanceID ItemChildCount Last_x0020_Modified
LinkFilename LinkFilename2 LinkFilenameNoMenu LinkTitle LinkTitle2 UniqueId
LinkTitleNoMenu MetaInfo Modified Order PermMask ProgId ScopeId SelectTitle
ServerUrl SortBehavior SyncClientId WorkflowInstanceID owshiddenversion
WorkflowVersion _CopySource _EditMenuTableEnd _EditMenuTableStart
_EditMenuTableStart2 _HasCopyDestinations _IsCurrentVersion _UIVersion
_ModerationComments _ModerationStatus _UIVersionString'''.split()

try:
    import gnomekeyring as gk
    import glib
    glib.set_application_name(str('splist'))
except:
    pass

import os
import sys
import json
import docopt
import logging

if os.getenv('SP_DEBUG'):
    logging.basicConfig(level='DEBUG')

from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.runtime.client_request import ClientRequest
from office365.runtime.utilities.request_options import RequestOptions
from office365.sharepoint.client_context import ClientContext


class Site(object):
    """docstring for Site"""
    def __init__(self, url, username, password):
        super(Site, self).__init__()
        self.url = url
        self.username = username
        self.password = password
        self.ctx = self._getsite(url, username, password)

    @staticmethod
    def _getsite(url,username,password):
        ctx_auth = AuthenticationContext(url)
        if ctx_auth.acquire_token_for_user(username, password):
            return ClientContext(url, ctx_auth)
        else:
            logging.error(ctx_auth.get_last_error())

    def getlist(self, name):
        return SPList(self, name)


class SPList(object):
    """docstring for SPList"""
    def __init__(self, site, name):
        super(SPList, self).__init__()
        self.site = site
        self.name = name

    @property
    def _list(self):
        return self.site.ctx.web.lists.get_by_title(self.name)

    def get(self,name,field='Title'):
        items = self._list.get_items().filter("{1} eq '{0}'".format(name,field))
        self.site.ctx.load(items)
        self.site.ctx.execute_query()
        # return items
        if len(items):
            return SPLItem(self, items[0])

    def add(self, props):
        # props['__metadata'] = {'type': 'SP.Data.TasksListItem'}
        item = self._list.add_item(props)
        self.site.ctx.execute_query()
        if item:
            return SPLItem(self, item)
        logging.error("List item has not been created: '{0!r}'".format(props))


class SPLItem(object):
    """docstring for SPLItem"""
    def __init__(self, splist, item):
        super(SPLItem, self).__init__()
        self.splist = splist
        self.item = item

    def __getitem__(self, key):
        return self.item.properties[key]

    def __setitem__(self, key, value):
        self.item.set_property(key, value)
        return value

    def save(self):
        self.item.update()
        self.splist.site.ctx.execute_query()



def getsite(host, site, team, user, passwd='', **opts):
    burl = str('https://{host}/'.format(host=host))
    surl = '{burl}sites/{site}/{team}'.format(burl=burl,site=site,team=team)

    try:
        keys = gk.find_items_sync(gk.ITEM_GENERIC_SECRET,{str('username_value'):user,str('signon_realm'):burl})
        pwd = keys[0].secret if keys else ''
    except:
        pwd = ''

    return Site(surl, user, passwd or pwd)


def load_data(ifp=sys.stdin,ofp=sys.stderr,verbose=False):
    if ifp.isatty():
        raise IOError('Input is a tty')
    data = json.load(ifp)
    data = data if isinstance(data,(list,tuple)) else [data]
    if verbose:
        json.dump(data,ofp,indent=(None,4)[ofp.isatty()])
        print('',file=ofp)
    return data


# def cmd_get(lst, ofp=sys.stdout, verbose=False, **opts):
#     if opts['FIELD']:
#         vals = (getattr(r,opts['FIELD']) for r in lst.rows)
#         keys = sorted(set(
#             f if isinstance(f, Hashable) else json.dumps(f)
#             for f in vals if f))
#         ret = '\n'.join(keys[:int(opts['limit']) or len(keys)])
#     else:
#         filters = OMD(zip(opts['KEY'],opts['VALUE'])).todict(True).items()
#         # print(filters,file=sys.stderr)
#         fl = ['ID'] + sorted(set(lst.fields.keys()) - set(fields_to_ignore))
#         rows = (r for r in lst.rows 
#             if all(getattr(r,k) in v for k,v in filters))
#         if opts['json']:
#             ret = json.dumps([r.as_dict(field_names=fl) for r in rows],indent=[None,4][opts['pretty']],default='{0}'.format)
#         else:
#             fmt = ['','{0.id} {0.Created}: '][verbose]+'{0.name}'
#             ret = '\n'.join(fmt.format(row) for row in rows)
#     ofp.write(ret+'\n')


def cmd_add(lst, ifp=sys.stdin, ofp=sys.stdout, verbose=False, namefield='Title', **opts):
    data = load_data(ifp,ofp,verbose)
    if opts.get('uniquename'):
        names = set(d[namefield] for d in data)
        if len(names) < len(data):
            raise KeyError('Duplicate name[{0}] fields found'.format(namefield))
        for name in names:
            if lst.get(name):
                raise KeyError('name already in use: {0}'.format(name))
    [lst.add(d) for d in data]


def cmd_mod(lst, ifp=sys.stdin, ofp=sys.stdout, verbose=False, namefield='Title', **opts):
    data = load_data(ifp,ofp,verbose)
    for d in data:
        item = lst.get(d['ID' if 'ID' in d else namefield],namefield)
        if item:
            for k,v in d.items():
                if k!='ID':
                    if k==namefield:
                        if item[namefield] != v:
                            item[namefield] = v
                    else:
                        item[k] = v
            item.save()


def run(opts):
    # print(opts,file=sys.stderr)
    # return
    opts = dict((k.lstrip('-'),v) for k,v in opts.items())
    opts['passwd'] = opts['pass'] or os.environ.get('SP_PASS','')
    opts['verbose'] = opts['pretty'] or opts['verbose']
    site = getsite(**opts)
    opts['ifp'] = sys.stdin if opts['ifn']=='-' else open(opts['ifn'], 'r')
    opts['ofp'] = sys.stdout if opts['ofn']=='-' else open(opts['ofn'], 'w')
    if not opts['list']:
        if opts['verbose']:
            print("List printing disabled",file=sys.stderr)
        # list_lists(site, **opts)
    else:
        fn = globals()['cmd_{0[COMMAND]}'.format(opts)]
        # print(fn.__name__.split('_')[-1].title(),file=sys.stderr)
        fn(site.getlist(opts['list']), **opts)

def main():
    defs = dict((x,os.getenv(x,'')) for x in 'SP_HOST SP_SITE SP_TEAM SP_LIST SP_USER'.split())
    try:
        run(docopt.docopt(__doc__.format(**defs),version=__version__))
    except (KeyError,IOError) as e:
        print(e.message,file=sys.stderr)

if __name__ == '__main__':
    main()
